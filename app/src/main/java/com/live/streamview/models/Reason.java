package com.live.streamview.models;

import java.io.Serializable;

/**
 * Created by codegama on 17/10/17.
 */

public class Reason implements Serializable {

    private String id;
    private String title;

    public Reason(String id, String title) {
        this.id = id;
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
