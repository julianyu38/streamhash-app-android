package com.live.streamview.models;

import java.io.Serializable;

/**
 * Created by codegama on 21/9/17.
 */

public class Card implements Serializable {

    public String id;
    public String number;
    public String month;
    public String year;
    public String cvv;

    public String last4;
    public boolean is_default;

    public Card(String id, String last4, boolean is_default) {
        this.id = id;
        this.last4 = last4;
        this.is_default = is_default;
    }

    public Card(String id, String number, String month, String year, String cvv) {
        this.id = id;
        this.number = number;
        this.month = month;
        this.year = year;
        this.cvv = cvv;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getLast4() {
        return last4;
    }

    public void setLast4(String last4) {
        this.last4 = last4;
    }

    public boolean is_default() {
        return is_default;
    }

    public void setIs_default(boolean is_default) {
        this.is_default = is_default;
    }

    @Override
    public String toString() {
        return "Card{" +
                "id='" + id + '\'' +
                ", number='" + number + '\'' +
                ", month='" + month + '\'' +
                ", year='" + year + '\'' +
                ", cvv='" + cvv + '\'' +
                '}';
    }
}
