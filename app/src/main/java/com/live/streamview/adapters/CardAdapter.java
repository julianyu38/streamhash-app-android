package com.live.streamview.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;


import com.live.streamview.R;
import com.live.streamview.utils.PostHelper;
import com.live.streamview.utils.SharedPref;
import com.live.streamview.utils.URLUtils;
import com.live.streamview.models.Card;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

/**
 * Created by codegama on 21/9/17.
 */

public class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder>{

    private Context context;
    private List<Card> cards;
    LayoutInflater inflater;

    RadioButton lastChecked;
    int lastCheckedPos;

    public CardAdapter(Context context, List<Card> cards) {
        this.context = context;
        this.cards = cards;
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.view_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        if (cards.size() > 0 ) {
            final Card card = cards.get(position);
            holder.last4.setText(context.getResources().getString(R.string.cardpadding) + "" +card.getLast4());
            holder.is_default.setChecked(card.is_default());
            holder.is_default.setTag(position);

            if (position == 0 && cards.get(0).is_default() && holder.is_default.isChecked()) {
                lastChecked = holder.is_default;
                lastCheckedPos = 0;
            }

            holder.is_default.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    RadioButton b = (RadioButton) view;
                    int clickedPos = ((Integer) b.getTag());
                    if (b.isChecked()) {
                        if (lastChecked != null) {
                            lastChecked.setChecked(false);
                            cards.get(lastCheckedPos).setIs_default(false);
                        }
                        makeDefaultCard(cards.get(clickedPos).getId());
                        lastChecked = b;
                        lastCheckedPos = clickedPos;
                    }
                    else{
                        lastChecked = null;
                        cards.get(clickedPos).setIs_default(b.isChecked());
                    }
                }
            });

            holder.delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    new AlertDialog.Builder(context)
                            .setMessage("Are you sure to delete the card - "+ card.getLast4() +"?")
                            .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    deleteCard(card.getId(), holder.getAdapterPosition());
                                }
                            })
                            .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                }
                            })
                            .create().show();
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return cards.size();
    }

    private void makeDefaultCard(final String cardId) {
        new AsyncTask<JSONObject, JSONObject, JSONObject>() {
            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", SharedPref.getKey(context, "ID"));
                    jsonObject.put("token", SharedPref.getKey(context, "TOKEN"));
                    jsonObject.put("card_id", cardId);

                    PostHelper postHelper = new PostHelper(context);
                    return postHelper.Post(URLUtils.defaultCard, jsonObject.toString());
                }
                catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(JSONObject obj) {
                super.onPostExecute(obj);
                Log.e("Response", obj.toString());
                if (obj.optString("success").equals("true")) {
                    JSONObject card = obj.optJSONObject("data");
                    SharedPref.putKey(context, "token", card.optString("token"));
                }
            }
        }.execute();
    }

    private void deleteCard(final String cardId, final int position) {
        new AsyncTask<JSONObject, JSONObject, JSONObject>() {
            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", SharedPref.getKey(context, "ID"));
                    jsonObject.put("token", SharedPref.getKey(context, "TOKEN"));
                    jsonObject.put("card_id", cardId);
                    jsonObject.put("position", position);
                    PostHelper postHelper = new PostHelper(context);
                    return postHelper.Post(URLUtils.deleteCard, jsonObject.toString());
                }
                catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(JSONObject obj) {
                super.onPostExecute(obj);
                Log.e("Response", obj.toString());
                if (obj.optString("success").equals("true")) {
                    JSONObject card = obj.optJSONObject("data");
                    removeAtPosition(card.optInt("position"));
                    SharedPref.putKey(context, "token", card.optString("token"));
                }
                else{
                    Toast.makeText(context, obj.optString("error_messages"), Toast.LENGTH_SHORT).show();
                }
            }
        }.execute();
    }

    private void removeAtPosition(int position) {
        cards.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, cards.size());
    }

//    @Override
//    public void onTaskCompleted(String response, int serviceCode) {
//        switch (serviceCode) {
//            case Util.API_CODE.DEFAULT_CARD_CODE:
//                try {
//                    JSONObject obj = new JSONObject(response);
//                    if (obj.optString(Util.SUCCESS).equals(Util.TRUE)) {
//                        JSONObject card = obj.optJSONObject("data");
//                        PrefHelper.getInstance().setToken(card.optString("token"));
//                    }
//                }
//                catch (JSONException e) {
//                    e.printStackTrace();
//                }
//                break;
//            case Util.API_CODE.DELETE_CARD_CODE:
//                try {
//                    JSONObject obj = new JSONObject(response);
//                    if (obj.optString(Util.SUCCESS).equals(Util.TRUE)) {
//                        JSONObject card = obj.optJSONObject("data");
//                        PrefHelper.getInstance().setToken(card.optString("token"));
//                    }
//                }
//                catch (JSONException e) {
//                    e.printStackTrace();
//                }
//        }
//    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView last4;
        public RadioButton is_default;
        public ImageButton delete;

        public ViewHolder(View itemView) {
            super(itemView);
            last4 = (TextView) itemView.findViewById(R.id.last4);
            is_default = (RadioButton) itemView.findViewById(R.id.is_default);
            delete = (ImageButton) itemView.findViewById(R.id.delete);
        }
    }
}
