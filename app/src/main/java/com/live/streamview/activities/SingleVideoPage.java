package com.live.streamview.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.ViewTreeObserver;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.like.LikeButton;
import com.like.OnLikeListener;
import com.live.streamview.ExoPlayerActivity;
import com.live.streamview.FullVideoActivity;
import com.live.streamview.R;
import com.live.streamview.models.CommentsItemModel;
import com.live.streamview.utils.CustomDialog;
import com.live.streamview.utils.GetHelper;
import com.live.streamview.utils.PostHelper;
import com.live.streamview.utils.SharedPref;
import com.live.streamview.utils.UIUtils;
import com.live.streamview.utils.URLUtils;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.live.streamview.activities.PlansActivity.PAYPAL_REQUEST_CODE;
import static com.live.streamview.activities.PlansActivity.config;

public class SingleVideoPage extends AppCompatActivity {
    private JSONObject video;
    private LinearLayout fullLayout;
    private int rating_value;
    private NestedScrollView scrollView;
    private CharSequence[] reasons;
    private String videoID = "", trailerVideo = "", image = "",
            youtube_video_id = "", shareUrl = "", wish_status = "", embedLink = "", amount = "";
    private boolean pay_per_view_status = false;
    private TextView videoName, videoDes, add_wishList, comment, share, paidLayout, publish_time, likes;
    private String videoType, trailerSubTitle, mainSubTitle;
    private ImageButton popup;
    String main_video;
    JSONObject jsonObject;
    private RatingBar videoRating;
    private ProgressBar progressBar;
    private ImageView play_btn;
    private Button play_full_video;
    private LikeButton like;
    private Toolbar toolbar;
    private Dialog dialog;

    private Drawable.Callback drawableCallack = new Drawable.Callback() {
        @Override
        public void invalidateDrawable(@NonNull Drawable drawable) {
            getSupportActionBar().setBackgroundDrawable(drawable);
        }

        @Override
        public void scheduleDrawable(@NonNull Drawable drawable, @NonNull Runnable runnable, long l) {

        }

        @Override
        public void unscheduleDrawable(@NonNull Drawable drawable, @NonNull Runnable runnable) {

        }
    };

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_video_page);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final ColorDrawable drawable = new ColorDrawable(ContextCompat.getColor(this, R.color.colorPrimary));
        drawable.setAlpha(0);
        drawable.setCallback(drawableCallack);

        getSupportActionBar().setTitle("");
        getSupportActionBar().setBackgroundDrawable(drawable);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent payintent = new Intent(this, PayPalService.class);
        payintent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        startService(payintent);

        Intent intent = getIntent();
        videoID = intent.getStringExtra("videoID");
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        play_btn = (ImageView) findViewById(R.id.play_btn);
        progressBar.setVisibility(View.VISIBLE);
        play_btn.setVisibility(View.GONE);
        popup = (ImageButton) findViewById(R.id.popup);
        videoRating = (RatingBar) findViewById(R.id.videoRatings);
        like = (LikeButton) findViewById(R.id.thumbbutton);
        share = (TextView) findViewById(R.id.share_text);
        comment = (TextView) findViewById(R.id.comment);
        play_full_video = (Button) findViewById(R.id.full_video);
        scrollView = (NestedScrollView) findViewById(R.id.scrollView);
        paidLayout = (TextView) findViewById(R.id.paidLayout);
        publish_time = (TextView) findViewById(R.id.publish_time);
        likes = (TextView) findViewById(R.id.likes);

        fullLayout = (LinearLayout) findViewById(R.id.full_layout);
        videoName = (TextView) findViewById(R.id.video_headLine);
        videoDes = (TextView) findViewById(R.id.des);
        add_wishList = (TextView) findViewById(R.id.add_wishList);

        loadingComment();

        add_wishList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String addWishList = "wishList";
                postValue(addWishList, progressBar, "", wish_status);
            }
        });

        scrollView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                int scrollY = scrollView.getScrollY();
                int boundY = 230;
                float ratio = (float) (scrollY / boundY);
                drawable.setAlpha((int) (ratio * 255));
            }
        });

        like.setOnLikeListener(new OnLikeListener() {
            @Override
            public void liked(LikeButton likeButton) {
                likeVideo();
                likeButton.setLiked(true);
            }

            @Override
            public void unLiked(LikeButton likeButton) {
                unLikeVideo();
                likeButton.setLiked(false);
            }
        });

        popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopupMenu(view);
            }
        });

        FloatingActionButton comments = (FloatingActionButton) findViewById(R.id.post_comment);
        comments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final CustomDialog customDialog = new CustomDialog(SingleVideoPage.this);
                customDialog.setContentView(R.layout.comment_dialog);

               /* RatingBar give_rating = (RatingBar) customDialog.findViewById(R.id.videoRatings);
                give_rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                    @Override
                    public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {

                        rating_value = (int) v;
                        String rating = "Your rating : " + rating_value;
                        UIUtils.showToastMsg(SingleVideoPage.this, rating);
                    }
                });*/

                ImageView imageView = (ImageView) customDialog.findViewById(R.id.banner_dialog);
                Glide.with(SingleVideoPage.this).load(image).centerCrop()
                        .diskCacheStrategy(DiskCacheStrategy.ALL).crossFade(50).error(R.drawable.ic_profile).into(imageView);

                final EditText editText = (EditText) customDialog.findViewById(R.id.postComment);
                Button comment_ok = (Button) customDialog.findViewById(R.id.ok);
                comment_ok.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String edit_string = editText.getText().toString();
                        if (edit_string.length() > 0) {
                            String addWishList = "";
                            postValue(addWishList, progressBar, edit_string, "");
                            customDialog.dismiss();
                        } else {
                            UIUtils.showToastMsg(SingleVideoPage.this, "Write the good comments.");
                        }
                    }
                });
                customDialog.show();
            }
        });

        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, shareUrl);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        });
    }

    private void showPopupMenu(final View view){
        final ClipboardManager manager = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        PopupMenu popupMenu = new PopupMenu(this, view);
        popupMenu.getMenuInflater().inflate(R.menu.popup_single_video, popupMenu.getMenu());

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()){
                    case R.id.embedlink:
                        ClipData data = ClipData.newPlainText("Embedded Link", embedLink);
                        manager.setPrimaryClip(data);
                        UIUtils.showToastMsg(SingleVideoPage.this, "Link has been copied to your clipboard");
                        return true;
                    case R.id.report:
                        showSpamDialog();
                        return true;
                }
                return false;
            }
        });
        popupMenu.show();
    }

    private void showSpamDialog() {
        new AsyncTask<JSONObject, JSONObject, JSONObject>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {
                try {
                    String url = URLUtils.spamReasons;
                    GetHelper getHelper = new GetHelper(SingleVideoPage.this);
                    return getHelper.run(url);
                }
                catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(JSONObject object) {
                super.onPostExecute(object);
                Log.e("SPAM REASONS", object.toString());
                progressBar.setVisibility(View.GONE);
                if (object != null) {
                    JSONArray data = object.optJSONArray("data");
                    if (data != null && data.length() > 0) {
                        reasons = new CharSequence[data.length()];
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject obj = data.optJSONObject(i);
                            reasons[i] = obj.optString("value");
                        }
                    }
                }
                if (reasons != null && reasons.length > 0) {
                    new AlertDialog.Builder(SingleVideoPage.this)
                            .setTitle("Reason to Report")
                            .setItems(reasons, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    markAsSpam(reasons[i].toString());
                                }
                            })
                            .create().show();
                }
                else {
                    markAsSpam("Nothing");
                }
            }
        }.execute();
    }

    private void markAsSpam(final String reason) {
        new AsyncTask<JSONObject, JSONObject, JSONObject>() {

            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", SharedPref.getKey(SingleVideoPage.this, "ID"));
                    jsonObject.put("token", SharedPref.getKey(SingleVideoPage.this, "TOKEN"));
                    jsonObject.put("sub_profile_id", SharedPref.getKey(SingleVideoPage.this, "SUB_PROFILE"));
                    jsonObject.put("admin_video_id", videoID);
                    jsonObject.put("reason", reason);

                    PostHelper postHelper = new PostHelper(SingleVideoPage.this);
                    return postHelper.Post(URLUtils.addSpamVideos, jsonObject.toString());

                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(JSONObject object) {
                super.onPostExecute(object);
                if (object != null) {
                    if (object.optString("success").equals("true")) {
                        UIUtils.showToastMsg(SingleVideoPage.this, object.optString("message"));
                        popup.setVisibility(View.GONE);
                    }
                    else {
                        UIUtils.showToastMsg(SingleVideoPage.this, object.optString("error_messages"));
                    }
                }
            }
        }.execute();
    }

    private void likeVideo() {
        new AsyncTask<JSONObject, JSONObject, JSONObject>() {

            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", SharedPref.getKey(SingleVideoPage.this, "ID"));
                    jsonObject.put("token", SharedPref.getKey(SingleVideoPage.this, "TOKEN"));
                    jsonObject.put("sub_profile_id", SharedPref.getKey(SingleVideoPage.this, "SUB_PROFILE"));
                    jsonObject.put("admin_video_id", videoID);

                    PostHelper postHelper = new PostHelper(SingleVideoPage.this);
                    return postHelper.Post(URLUtils.likeVideos, jsonObject.toString());

                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    private void unLikeVideo(){
        new AsyncTask<JSONObject, JSONObject, JSONObject>() {

            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", SharedPref.getKey(SingleVideoPage.this, "ID"));
                    jsonObject.put("token", SharedPref.getKey(SingleVideoPage.this, "TOKEN"));
                    jsonObject.put("sub_profile_id", SharedPref.getKey(SingleVideoPage.this, "SUB_PROFILE"));
                    jsonObject.put("admin_video_id", videoID);

                    PostHelper postHelper = new PostHelper(SingleVideoPage.this);
                    return postHelper.Post(URLUtils.disLikeVideos, jsonObject.toString());

                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    private void addHistory(final String admin_video_id) {
        new AsyncTask<JSONObject, JSONObject, JSONObject>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", SharedPref.getKey(SingleVideoPage.this, "ID"));
                    jsonObject.put("token", SharedPref.getKey(SingleVideoPage.this, "TOKEN"));
                    jsonObject.put("sub_profile_id", SharedPref.getKey(SingleVideoPage.this, "SUB_PROFILE"));
                    jsonObject.put("admin_video_id", admin_video_id);

                    Log.e("addHistoryList", "" + jsonObject);
                    PostHelper postHelper = new PostHelper(SingleVideoPage.this);
                    return postHelper.Post(URLUtils.addHistory, jsonObject.toString());

                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(JSONObject jsonObject) {
                super.onPostExecute(jsonObject);
                progressBar.setVisibility(View.GONE);
                Log.e("addWishList", "" + jsonObject);
                if (jsonObject != null) {
                    if (jsonObject.optString("success").equalsIgnoreCase("true")) {

                        //UIUtils.showToastMsg(SingleVideoPage.this, jsonObject.optString("message"));
                    } else {
                        UIUtils.showToastMsg(SingleVideoPage.this, jsonObject.optString("error"));
                        if (jsonObject.optString("error_code").equalsIgnoreCase("104")) {
                            Intent intent = new Intent(SingleVideoPage.this, Login.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    }
                } else {

                    UIUtils.showToast(SingleVideoPage.this, R.string.con_timeout);
                }

            }
        }.execute();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_single_video, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            case R.id.action_settings:
                Intent intent = new Intent(this, SearchActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                return true;
            case R.id.action_share:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_SUBJECT, videoName.getText().toString().trim());
                sendIntent.putExtra(Intent.EXTRA_TEXT, shareUrl);
                sendIntent.setType("text/plain");
                startActivity(Intent.createChooser(sendIntent, "Share"));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void loadingComment() {
        new AsyncTask<JSONObject, JSONObject, JSONObject>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", SharedPref.getKey(SingleVideoPage.this, "ID"));
                    jsonObject.put("token", SharedPref.getKey(SingleVideoPage.this, "TOKEN"));
                    jsonObject.put("admin_video_id", videoID);

                    PostHelper postHelper = new PostHelper(SingleVideoPage.this);
                    return postHelper.Post(URLUtils.singleVideo, jsonObject.toString());
                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(JSONObject jsonObject) {
                super.onPostExecute(jsonObject);
                progressBar.setVisibility(View.GONE);
                Log.e("jsonObj", "" + jsonObject);
                SingleVideoPage.this.jsonObject = jsonObject;
                if (jsonObject != null) {
                    fullLayout.setVisibility(View.VISIBLE);
                    if (jsonObject.optString("success").equalsIgnoreCase("true")) {
                        video = jsonObject.optJSONObject("video");
                        shareUrl = jsonObject.optString("share_link");
                        main_video = jsonObject.optString("main_video");
                        mainSubTitle = jsonObject.optString("video_subtitle");
                        trailerSubTitle = jsonObject.optString("trailer_subtitle");
                        videoName.setText(video.optString("title"));
                        videoDes.setText(video.optString("description"));
                        likes.setText(video.optString("likes") + " Likes");
                        publish_time.setText(video.optString("publish_time"));
                        image = video.optString("default_image");
                        embedLink = jsonObject.optString("trailer_embed_link");
                        like.setLiked(video.optInt("is_liked") == 1);
                        Glide.with(SingleVideoPage.this).load(image)
                                .diskCacheStrategy(DiskCacheStrategy.ALL).fitCenter()
                                .into((ImageView) findViewById(R.id.thumbnail));
                        findViewById(R.id.thumbnail).setVisibility(View.VISIBLE);
                        float rating = Float.parseFloat(video.optString("ratings"));
                        videoRating.setRating(rating);
                        pay_per_view_status = jsonObject.optBoolean("pay_per_view_status");
                        trailerVideo = video.optString("trailer_video");
                        if (jsonObject.optString("wishlist_status").equalsIgnoreCase("1")) {
                            add_wishList.setText(R.string.added_to_mylist);
                            wish_status = jsonObject.optString("wishlist_status");
                        } else if (jsonObject.optString("wishlist_status").equalsIgnoreCase("0")) {
                            add_wishList.setText(R.string.add_to_mylist);
                            wish_status = jsonObject.optString("wishlist_status");
                        }
                        videoType = video.optString("video_type");
                        amount = video.optString("amount");
                        if (!pay_per_view_status) {
                            paidLayout.setVisibility(View.VISIBLE);
                            paidLayout.setText("You need to pay $" + amount + " to watch the Full Video");
                        }
                        else {
                            paidLayout.setVisibility(View.GONE);
                        }
                        playVideo();
                        JSONArray comment_array = jsonObject.optJSONArray("comments");
                        if (comment_array != null) {
                            if (comment_array.length() > 0)
                                comment.setVisibility(View.VISIBLE);
                            ArrayList<CommentsItemModel> commentsItemModels = new ArrayList<>();
                            for (int i = 0; i < comment_array.length(); i++) {
                                JSONObject object = comment_array.optJSONObject(i);
                                String userID = object.optString("user_id");
                                String username = object.optString("username");
                                String user_picture = object.optString("user_picture");
                                String com_rating = object.optString("rating");
                                String comment = object.optString("comment");
                                commentsItemModels.add(new CommentsItemModel(userID, username, user_picture, com_rating, comment));
                            }
                            RecyclerView recyclerView = (RecyclerView) findViewById(R.id.comment_list);
                            recyclerView.setHasFixedSize(true);
                            recyclerView.setNestedScrollingEnabled(false);
                            recyclerView.setLayoutManager(new LinearLayoutManager(SingleVideoPage.this));
                            CommentAdapter commentAdapter = new CommentAdapter(commentsItemModels);
                            recyclerView.setAdapter(commentAdapter);
                        }

                    } else {
                        UIUtils.showToastMsg(SingleVideoPage.this, jsonObject.optString("error"));
                        if (jsonObject.optString("error_code").equalsIgnoreCase("104")) {
                            Intent intent = new Intent(SingleVideoPage.this, Login.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    }
                } else {
                    UIUtils.showToast(SingleVideoPage.this, R.string.con_timeout);
                }

            }
        }.execute();
    }

    private void playVideo() {
        if (videoType.equalsIgnoreCase("2")) {
            String youtube_video = video.optString("trailer_video");
            String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";
            play_btn.setVisibility(View.VISIBLE);
            Pattern compiledPattern = Pattern.compile(pattern);
            Matcher matcher = compiledPattern.matcher(youtube_video);

            if (matcher.find()) {
                youtube_video_id = matcher.group();
            }

            playFullVideo(jsonObject.optString("user_type"), play_full_video, main_video, video.optString("video_type"));

            play_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addHistory(video.optString("admin_video_id"));
                    Intent video = new Intent(SingleVideoPage.this, FullVideoActivity.class);
                    video.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    video.putExtra("video_type", "2");
                    video.putExtra("video_URL", youtube_video_id);
                    startActivity(video);
                    Log.e("youtube_video_id", "" + youtube_video_id);
                }
            });

        } else if (videoType.equalsIgnoreCase("1") || videoType.equalsIgnoreCase("3")) {
            play_btn.setVisibility(View.VISIBLE);
            playFullVideo(jsonObject.optString("user_type"), play_full_video, main_video, video.optString("video_type"));

            play_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent videoIntent = new Intent(SingleVideoPage.this, ExoPlayerActivity.class);
                    videoIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    videoIntent.putExtra("video_type", video.optString("video_type"));
                    videoIntent.putExtra("video_URL", trailerVideo);
                    videoIntent.putExtra("video_name", videoName.getText().toString());
                    videoIntent.putExtra("video_subtitle", trailerSubTitle);
                    videoIntent.putExtra("admin_video_id", videoID);
                    startActivity(videoIntent);
                    addHistory(video.optString("admin_video_id"));
                }
            });
        }
    }

    private void showPaymentDialog(final String amount) {
        dialog = new Dialog(this, R.style.AppTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_invoice);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        ImageButton close = (ImageButton) dialog.findViewById(R.id.close);
        ImageButton paypal = (ImageButton) dialog.findViewById(R.id.paypal);
        ImageButton stripe = (ImageButton) dialog.findViewById(R.id.stripe);
        TextView title = (TextView) dialog.findViewById(R.id.title);
        TextView price = (TextView) dialog.findViewById(R.id.price);
        TextView months = (TextView) dialog.findViewById(R.id.months);
        title.setText(videoName.getText().toString());
        price.setText(amount);
        dialog.findViewById(R.id.monthsLayout).setVisibility(View.GONE);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.cancel();
                onBackPressed();
            }
        });
        paypal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                if (Double.parseDouble(amount) == 0) {
                    showPaymentConfirmationDialog("Free trial Plan");
                    makeAPing(videoID, "Free trial Plan");
                }
                else {
                    getPaypalPayment(amount, videoName.getText().toString());
                }
            }
        });
        stripe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                makeStripePayment(videoID);
            }
        });
        dialog.show();
    }

    private void makeStripePayment(final String id) {
        new AsyncTask<JSONObject, JSONObject, JSONObject>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {
                try {
                    JSONObject object = new JSONObject();
                    object.put("id", SharedPref.getKey(SingleVideoPage.this, "ID"));
                    object.put("token", SharedPref.getKey(SingleVideoPage.this, "TOKEN"));
                    object.put("sub_profile_id", SharedPref.getKey(SingleVideoPage.this, "SUB_PROFILE"));
                    object.put("admin_video_id", id);
                    PostHelper postHelper = new PostHelper(SingleVideoPage.this);
                    return postHelper.Post(URLUtils.stripePayPerView, object.toString());
                }
                catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(JSONObject object) {
                super.onPostExecute(object);
                progressBar.setVisibility(View.GONE);
                Log.e("STRIPE", object.toString());
                if (object != null) {
                    if (object.optString("success").equals("true")) {
                        UIUtils.showToastMsg(SingleVideoPage.this, object.optString("message"));
                        JSONObject data = object.optJSONObject("data");
                        SharedPref.putKey(SingleVideoPage.this, "TOKEN", data.optString("token"));
                        fullLayout.setVisibility(View.VISIBLE);
                        playVideo();
                    }
                    else{
                        UIUtils.showToastMsg(SingleVideoPage.this, object.optString("error_messages"));
                        Intent intent = new Intent(SingleVideoPage.this, MainActivity.class);
                        intent.putExtra("changeToPayment", true);
                        intent.putExtra("navigation", "payments");
                        startActivity(intent);
                    }
                }
            }
        }.execute();
    }

    private void getPaypalPayment(String amount, String title) {
        PayPalPayment payment = new PayPalPayment(new BigDecimal(String.valueOf(amount)), "USD", title,
                PayPalPayment.PAYMENT_INTENT_SALE);
        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, config);
        intent.putExtra(PaymentActivity.EXTRA_PAYMENT, payment);
        startActivityForResult(intent, PAYPAL_REQUEST_CODE);
    }

    private void showPaymentConfirmationDialog(String paymentDetails){
        final Dialog dialog = new Dialog(this, R.style.AppTheme);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_payment_confirmation);
        dialog.setCancelable(false);
        TextView paidamount = (TextView) dialog.findViewById(R.id.amount);
        TextView status = (TextView) dialog.findViewById(R.id.status);
        TextView paidid = (TextView) dialog.findViewById(R.id.paymentid);
        try {
            JSONObject details = new JSONObject(paymentDetails);
            paidamount.setText(amount);
            status.setText(details.optJSONObject("response").optString("state"));
            paidid.setText(details.optJSONObject("response").optString("id"));
            if (status.getText().toString().equalsIgnoreCase("approved")) {
                makeAPing(videoID, details.optJSONObject("response").optString("id"));
            }
            else{
                UIUtils.showToastMsg(SingleVideoPage.this, "Payment unsuccessful");
            }
        }
        catch (JSONException e){
            e.printStackTrace();
            paidamount.setText(amount);
            status.setText("Approved");
            paidid.setText(paymentDetails);
        }
        Button done = (Button) dialog.findViewById(R.id.button_start);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialog != null && dialog.isShowing()) dialog.cancel();
            }
        });
        dialog.show();
    }

    private void makeAPing(final String id, final String payment_id){
        new AsyncTask<JSONObject, JSONObject, JSONObject>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {
                try {
                    JSONObject object = new JSONObject();
                    object.put("id", SharedPref.getKey(SingleVideoPage.this, "ID"));
                    object.put("token", SharedPref.getKey(SingleVideoPage.this, "TOKEN"));
                    object.put("payment_id", payment_id);
                    object.put("admin_video_id", id);
                    PostHelper postHelper = new PostHelper(SingleVideoPage.this);
                    return postHelper.Post(URLUtils.paypalPayPerView, object.toString());
                }
                catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(JSONObject object) {
                super.onPostExecute(object);
                progressBar.setVisibility(View.GONE);
                Log.e("PAYPAL", object.toString());
                if (object != null) {
                    if (object.optString("success").equals("true")) {
                        UIUtils.showToastMsg(SingleVideoPage.this, object.optString("message"));
                        JSONObject data = object.optJSONObject("data");
                        SharedPref.putKey(SingleVideoPage.this, "TOKEN", data.optString("token"));
                        fullLayout.setVisibility(View.VISIBLE);
                        playVideo();
                    }
                }
            }
        }.execute();
    }

    private void playFullVideo(final String user_type, Button play_full_video, final String main_video, final String type) {
        play_full_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!pay_per_view_status) {
                    showPaymentDialog(video.optString("amount"));
                }
                else{
                    if (user_type.equalsIgnoreCase("0")) {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(SingleVideoPage.this);
                        builder.setMessage("Your are not paid user, if you paid after you can watch full video.");
                        builder.setCancelable(false);
                        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(final DialogInterface dialogInterface, int i) {
                                Intent intent = new Intent(SingleVideoPage.this, PlansActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                intent.putExtra("title", "View Plans");
                                intent.putExtra("type", 1);
                                startActivity(intent);
                                dialogInterface.dismiss();

                            }
                        });
                        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        builder.show();
                    } else {

                        if (type.equalsIgnoreCase("2")) {
                            String pattern = "(?<=watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";
                            String videoURL = "";
                            Pattern compiledPattern = Pattern.compile(pattern);
                            Matcher matcher = compiledPattern.matcher(main_video);

                            if (matcher.find()) {
                                videoURL = matcher.group();
                                Log.e("MainVideo", "" + videoURL);

                                Intent video = new Intent(SingleVideoPage.this, FullVideoActivity.class);
                                video.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                video.putExtra("video_type", type);
                                video.putExtra("video_URL", videoURL);
                                startActivity(video);
                            }
                        }
                        else {
                            Intent video = new Intent(SingleVideoPage.this, ExoPlayerActivity.class);
                            video.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            video.putExtra("video_type", type);
                            video.putExtra("video_URL", main_video);
                            video.putExtra("video_name", videoName.getText().toString());
                            video.putExtra("video_subtitle", mainSubTitle);
                            video.putExtra("admin_video_id", videoID);
                            startActivity(video);
                        }

                    }
                }
            }
        });

    }


    private void postValue(final String addWishList, final ProgressBar progressBar, final String comment, final String status) {
        new AsyncTask<JSONObject, JSONObject, JSONObject>() {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            protected JSONObject doInBackground(JSONObject... jsonObjects) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("id", SharedPref.getKey(SingleVideoPage.this, "ID"));
                    jsonObject.put("token", SharedPref.getKey(SingleVideoPage.this, "TOKEN"));
                    jsonObject.put("admin_video_id", video.optString("admin_video_id"));
                    if (addWishList.equalsIgnoreCase("wishList")) {
                        jsonObject.put("status", status);
                        Log.e("addWishList", "" + jsonObject);
                        PostHelper postHelper = new PostHelper(SingleVideoPage.this);
                        return postHelper.Post(URLUtils.addWishlist, jsonObject.toString());
                    } else {
                        jsonObject.put("rating", rating_value);
                        jsonObject.put("comments", comment);
                        Log.e("rating_comment", "" + jsonObject);
                        PostHelper postHelper = new PostHelper(SingleVideoPage.this);
                        return postHelper.Post(URLUtils.userRating, jsonObject.toString());
                    }

                } catch (JSONException | IOException e) {
                    e.printStackTrace();
                }
                return null;
            }

            @Override
            protected void onPostExecute(JSONObject jsonObject) {
                super.onPostExecute(jsonObject);
                progressBar.setVisibility(View.GONE);
                Log.e("addWishList", "" + jsonObject);
                if (jsonObject != null) {
                    if (jsonObject.optString("success").equalsIgnoreCase("true")) {
                        if (addWishList.equalsIgnoreCase("wishList") && jsonObject.optString("wishlist_status").equalsIgnoreCase("0")) {
                            add_wishList.setText(R.string.add_to_mylist);
                            wish_status = jsonObject.optString("wishlist_status");
                        } else if (addWishList.equalsIgnoreCase("wishList") && jsonObject.optString("wishlist_status").equalsIgnoreCase("1")) {
                            add_wishList.setText(R.string.added_to_mylist);
                            wish_status = jsonObject.optString("wishlist_status");
                        } else if (addWishList.isEmpty()) {
                            loadingComment();
                        }

                        UIUtils.showToastMsg(SingleVideoPage.this, jsonObject.optString("message"));
                    } else {
                        UIUtils.showToastMsg(SingleVideoPage.this, jsonObject.optString("error"));
                        if (jsonObject.optString("error_code").equalsIgnoreCase("104")) {
                            Intent intent = new Intent(SingleVideoPage.this, Login.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                        }
                    }
                } else {

                    UIUtils.showToast(SingleVideoPage.this, R.string.con_timeout);
                }

            }
        }.execute();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            // Retry initialization if user performed a recovery action
//            getYouTubePlayerProvider().initialize(API, this);
        }
        if (requestCode == PAYPAL_REQUEST_CODE){
            if (resultCode == Activity.RESULT_OK){
                PaymentConfirmation confirm = data.getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);

                if (confirm != null){
                    try {
                        String paymentDetails = confirm.toJSONObject().toString(4);
                        Log.d("PaypalPayment", paymentDetails);
                        showPaymentConfirmationDialog(paymentDetails);
                    }
                    catch (JSONException e){
                        e.printStackTrace();
                    }
                }
            }
            else if (resultCode == Activity.RESULT_CANCELED){
                if (dialog != null && dialog.isShowing()) { dialog.cancel(); }
            }
        }
        else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID){
            Log.e("PaypalError", "Invalid Payment or Paypal Configuration was submitted");
        }
    }

//    private YouTubePlayer.Provider getYouTubePlayerProvider() {
//        return (YouTubePlayerView) findViewById(R.id.youtube_view);
//    }


    private class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.MyViewHolder> {
        ArrayList<CommentsItemModel> itemModels;

        public CommentAdapter(ArrayList<CommentsItemModel> commentsItemModels) {
            this.itemModels = commentsItemModels;
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(SingleVideoPage.this).inflate(R.layout.comment_list_item, parent, false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            Glide.with(SingleVideoPage.this).load(itemModels.get(position).getUser_picture()).diskCacheStrategy(DiskCacheStrategy.ALL).crossFade(50).error(R.drawable.ic_profile).into(holder.imageView);
            holder.rating_des.setText(itemModels.get(position).getComment());
            holder.user_name.setText(itemModels.get(position).getUsername());
            // holder.ratingBar.setRating(Float.parseFloat(itemModels.get(position).getRating()));
        }

        @Override
        public int getItemCount() {
            return itemModels.size();
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            CircularImageView imageView;
            TextView user_name, rating_des;
            //RatingBar ratingBar;

            public MyViewHolder(View itemView) {
                super(itemView);
                imageView = (CircularImageView) itemView.findViewById(R.id.comment_image);
                user_name = (TextView) itemView.findViewById(R.id.comment_user_name);
                rating_des = (TextView) itemView.findViewById(R.id.comment_des);
                //ratingBar = (RatingBar) itemView.findViewById(R.id.comment_rating);
            }
        }
    }
}
