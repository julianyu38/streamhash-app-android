package com.live.streamview.activities;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.live.streamview.R;
import com.live.streamview.utils.ConnectionHelper;
import com.live.streamview.utils.PostHelper;
import com.live.streamview.utils.SharedPref;
import com.live.streamview.utils.UIUtils;
import com.live.streamview.utils.URLUtils;
import com.live.streamview.adapters.UserGridAdapter;
import com.live.streamview.models.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ManageProfileActivity extends AppCompatActivity {

    private RecyclerView profiles;
    private List<User> users;
    private UserGridAdapter adapter;
    private ProgressBar progressBar;
    private boolean isEdit = false;
    private MenuItem item;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_profile);

        users = new ArrayList<>();
        profiles = (RecyclerView) findViewById(R.id.profiles);
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        profiles.setHasFixedSize(true);
        getUsers();
        profiles.setItemAnimator(new DefaultItemAnimator());
        profiles.setLayoutManager(new GridLayoutManager(this, 2));
        adapter = new UserGridAdapter(this, users);
        profiles.setAdapter(adapter);

        getSupportActionBar().setTitle("Manage Profiles");
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit:
                isEdit = true;
                adapter.setEditMode(true);
                this.item = item;
                item.setVisible(false);
                adapter.notifyDataSetChanged();
                UIUtils.showToastMsg(this, "Click the profile to Edit");
                break;
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (isEdit) {
            isEdit = false;
            item.setVisible(true);
            adapter.setEditMode(false);
            adapter.notifyDataSetChanged();
            return;
        }
        super.onBackPressed();
    }

    private void getUsers() {
        if (ConnectionHelper.isConnectingToInternet(this)) {
            new AsyncTask<JSONObject, JSONObject, JSONObject>() {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    progressBar.setVisibility(View.VISIBLE);
                }

                @Override
                protected JSONObject doInBackground(JSONObject... jsonObjects) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("id", SharedPref.getKey(ManageProfileActivity.this, "ID"));
                        jsonObject.put("token", SharedPref.getKey(ManageProfileActivity.this, "TOKEN"));

                        PostHelper postHelper = new PostHelper(ManageProfileActivity.this);
                        return postHelper.Post(URLUtils.activeProfiles, jsonObject.toString());

                    } catch (JSONException | IOException e) {
                        e.printStackTrace();
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(JSONObject object) {
                    super.onPostExecute(object);
                    progressBar.setVisibility(View.GONE);
                    Log.e("Profiles", object.toString());
                    if (object != null) {
                        if (object.optString("success").equalsIgnoreCase("true")) {
                            JSONArray data = object.optJSONArray("data");
                            for (int i = 0 ; i < data.length(); i++) {
                                JSONObject u = data.optJSONObject(i);
                                User user = new User(u.optString("id"), u.optString("name"), u.optString("picture"), u.optInt("status") == 1);
                                users.add(user);
                            }
                            users.add(new User("add", "Add Profile", "", false));
                            adapter.notifyDataSetChanged();
                        }
                        else {
                            UIUtils.showToastMsg(ManageProfileActivity.this, object.optString("error"));
                            if (object.optString("error_code").equalsIgnoreCase("104")) {
                                Intent intent = new Intent(ManageProfileActivity.this, Login.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                startActivity(intent);
                            }
                        }
                    } else {
                        UIUtils.showToast(ManageProfileActivity.this, R.string.con_timeout);
                    }
                }
            }.execute();
        }
        else {
            UIUtils.showToastMsg(this, "Need to Connect to the Internet");
            return;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return super.onCreateOptionsMenu(menu);
    }
}
